class ErrorBase extends Error {
  constructor (message, data) {
    super(message || "unexpected error");
    this.code = "UNEXPECTED_ERROR";
    this.httpStatus = 500;
    
    if (data) {
      this.data = data;
    }

    if ([ "demo", "production" ].indexOf(process.env.NODE_ENV) > -1) {
      this.stack = null;
    }
    
    if (ErrorBase.errorLogger) {
      ErrorBase.errorLogger(this);
    }
  }

  static setLogger (loggerCallback) {
    if (loggerCallback && typeof(loggerCallback) === "function") {
      ErrorBase.errorLogger = loggerCallback;
    }
  }
}

module.exports = ErrorBase;