"use strict";
const ForbiddenError = require("./technical/forbiddenError");
const NotFoundError = require("./technical/notFoundError");
const UnexpectedError = require("./technical/unexpectedError");
const UnauthorizedError = require("./technical/unauthorizedError");
const ValidationError = require("./technical/validationError");
const HostUnavailable = require("./technical/hostUnavailable");

module.exports = {
  ForbiddenError,
  NotFoundError,
  UnexpectedError,
  UnauthorizedError,
  ValidationError,
  HostUnavailable
};