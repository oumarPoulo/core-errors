let ErrorBase = require('../errorBase');

class ForbiddenError extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 403;
    this.code = "FORBIDDEN_OPERATION";
  }
}

module.exports = ForbiddenError;