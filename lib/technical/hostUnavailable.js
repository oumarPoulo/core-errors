let ErrorBase = require('../errorBase');

class HostUnavailable extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 503;
    this.code = "HOST_UNAVAILABLE";
  }
}

module.exports = HostUnavailable;