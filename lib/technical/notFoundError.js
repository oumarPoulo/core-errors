let ErrorBase = require('../errorBase');

class NotFoundError extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 404;
    this.code = "DATA_NOT_FOUND";
  }
}

module.exports = NotFoundError;