let ErrorBase = require('../errorBase');

class UnauthorizedError extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 401;
    this.code = "UNAUTHORIZED_OPERATION";
  }
}

module.exports = UnauthorizedError;