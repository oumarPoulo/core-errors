let ErrorBase = require('../errorBase');

class UnexpectedError extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 500;
    this.code = "UNEXPECTED_ERROR";
  }
}

module.exports = UnexpectedError;