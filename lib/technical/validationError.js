let ErrorBase = require('../errorBase');

class ValidationError extends ErrorBase {
  constructor (message, data) {
    super(message, data);
    this.httpStatus = 400;
    this.code = "DATA_NOT_VALID";
  }
}

module.exports = ValidationError;