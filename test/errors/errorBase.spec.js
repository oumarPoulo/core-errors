const assert = require("assert");
const ErrorBase = require("../../lib/errorBase");

describe("errorBase test", ()=> {
  it("should have a message when instanciate", ()=> {
    let message = "message";

    let error = new ErrorBase(message);

    assert.equal(error.message, message, "le message n'est pas présent");
  });
  
  it("should have a stack when instanciate and env is not defined", ()=> {
    let message = "message";

    let error = new ErrorBase(message);

    assert.equal(process.env.NODE_ENV, undefined, "env n'est pas dev");
    assert.notEqual(error.stack, null, "la stack n'est pas présente");
  });
});
